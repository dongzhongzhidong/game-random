import { random } from "../dist/index.js";
import testTree from "jest-test-tree/jest-test-tree.js";
const isFloat = (a) => (a + "").indexOf(".") !== -1;
const isBetween = (a, b, c) => a >= b && a < c;
const testTime = 1000;
testTree({
    random: [
        [
            "null",
            () => {
                return [...Array(testTime).keys()].map(() => random());
            },
            (res) => {
                res.forEach((i) => {
                    expect(i + "").toContain(".");
                    expect(i).toBeLessThan(1);
                    expect(i).toBeGreaterThanOrEqual(0);
                });
            },
        ],
        [
            "max",
            () => {
                return [...Array(testTime).keys()]
                    .map(() => random(5))
                    .every((i) => !isFloat(i) && isBetween(i, 0, 5));
            },
            (res) => expect(res).toBe(true),
        ],
        [
            "seed",
            () => {
                const target = random(5, "2233");
                return [...Array(testTime).keys()]
                    .map(() => random(5, "2233"))
                    .every((i) => !isFloat(i) && i === target);
            },
            (res) => expect(res).toBe(true),
        ],
        [
            "floating",
            () => {
                return [...Array(testTime).keys()]
                    .map(() => random(true))
                    .every((i) => isFloat(i));
            },
            (res) => expect(res).toBe(true),
        ],
        [
            "min,max",
            () => {
                return [...Array(testTime).keys()]
                    .map(() => random(1, 6))
                    .every((i) => !isFloat(i) && isBetween(i, 1, 6));
            },
            (res) => expect(res).toBe(true),
        ],
        [
            "max,floating",
            () => {
                return [...Array(testTime).keys()]
                    .map(() => random(5, true))
                    .every((i) => isFloat(i) && isBetween(i, 0, 5));
            },
            (res) => expect(res).toBe(true),
        ],
        [
            "max,seed",
            () => {
                const target = random(5, "Konghayao");
                return [...Array(testTime).keys()]
                    .map(() => random(5, "Konghayao"))
                    .every((i) => !isFloat(i) && i === target);
            },
            (res) => expect(res).toBe(true),
        ],
        [
            "min,max,floating",
            () => {
                return [...Array(testTime).keys()]
                    .map(() => random(5, 100, true))
                    .every((i) => isFloat(i) && isBetween(i, 5, 100));
            },
            (res) => expect(res).toBe(true),
        ],
        [
            "min,max,seed",
            () => {
                const target = random(100, "Konghayao");
                return [...Array(testTime).keys()]
                    .map(() => random(100, "Konghayao"))
                    .every((i) => !isFloat(i) && i === target);
            },
            (res) => expect(res).toBe(true),
        ],
        [
            "min,max,floating,seed",
            () => {
                const target = random(20, 100, true, "Konghayao");
                return [...Array(testTime).keys()]
                    .map(() => random(20, 100, true, "Konghayao"))
                    .every((i) => isFloat(i) && i === target);
            },
            (res) => expect(res).toBe(true),
        ],
    ],
});
