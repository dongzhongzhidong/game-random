import { sample, sampleSize } from "../dist/index.js";
import testTree from "jest-test-tree/jest-test-tree.js";

const testTime = 100;
const arr = [...Array(1000).keys()].map((i) => i + "");
const seed = "123232";
testTree({
    sample: [
        [
            "arr",
            () => {
                return [...Array(testTime).keys()].map(() => sample(arr));
            },
            (res) =>
                res.forEach((i) => {
                    expect(arr).toContain(i);
                }),
        ],
        [
            "arr, seed",
            () => {
                return [...Array(testTime).keys()].map((i) =>
                    sample(arr, seed)
                );
            },
            (res) => {
                const target = sample(arr, seed);
                res.forEach((i) => {
                    expect(i).toEqual(target);
                });
            },
        ],
    ],
    sampleSize: [
        [
            "null",
            () => [...Array(testTime).keys()].map((i) => sampleSize(arr)),
            (res) => {
                res.forEach((i) => {
                    expect(i.length).toBe(1);
                    expect(arr).toContain(i[0]);
                });
            },
        ],
        [
            "number",
            () => [...Array(testTime).keys()].map((i) => sampleSize(arr, 10)),
            (res) => {
                res.forEach((i) => {
                    expect(i.length).toBe(10);
                    i.forEach((ii) => {
                        expect(arr).toContain(ii);
                    });
                });
            },
        ],
        [
            "seed",
            () => [...Array(testTime).keys()].map((i) => sampleSize(arr, seed)),
            (res) => {
                const target = sampleSize(arr, seed);
                res.forEach((i) => {
                    expect(i.length).toBe(1);
                    expect(i).toEqual(target);
                });
            },
        ],
        [
            "number,seed",
            () =>
                [...Array(testTime).keys()].map((i) =>
                    sampleSize(arr, 10, seed)
                ),
            (res) => {
                const target = sampleSize(arr, 10, seed);
                res.forEach((i) => {
                    expect(i.length).toBe(10);
                    expect(i).toEqual(target);
                });
            },
        ],
    ],
});
