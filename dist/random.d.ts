declare function random(floating?: boolean): number;
declare function random(seed?: string): number;
declare function random(max: number, floating?: boolean): number;
declare function random(max: number, seed?: string): number;
declare function random(min: number, max: number, floating: boolean, seed?: string): number;
declare function random(min: number, max: number, seed?: string): number;
export { random };
