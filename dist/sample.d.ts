export declare function sample<T>(arr: T[], seed?: string): T;
declare function sampleSize<T>(collection: T[], seed?: string): T[];
declare function sampleSize<T>(collection: T[], n?: number): T[];
declare function sampleSize<T>(collection: T[], n: number, seed?: string): T[];
export { sampleSize };
